<?php
/**
 * @category 	Slider
 * @author 		Wilco
 * @since 		3-3-2017
 * @version 	1.0
 */

/**
 * Bruikbare functies (te overschrijven in functions.php)
 *
 * - minimal_slider_image_url(get_the_id())
 * - minimal_slider_image_id()
 * - the_content()
 * - the_title()
 * - the_author()
 * - the_ID()
 *
 * .slider-container:
 *		- .vertical-center
 *		- .horizontal-center
 * 
 */
?>

<?php if(minimal_slider_image_url(get_the_id() ) ) { 

$url = minimal_slider_image_url(get_the_id()); ?>

<li data-thumb="<?php echo $url; ?>" style='background-position: center;background-size: cover;background-repeat: no-repeat;background-image: url(<?php echo $url; ?>);'>

	<div class='slider-content-container vertical-center horizontal-center'>

		<div class='full-row'>

			<div class='slider-content'>

				<?php the_content(); ?>

			</div>

		</div>

	</div>

</li>

<?php } ?>