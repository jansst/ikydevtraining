<?php

// Retrieve the options
if(get_field('slider_animation_type_name','option') ) {

	$animation = get_field('slider_animation_type_name','option');
}else{

	$animation = '';
}

if(get_field('slider_easing_name','option') ) {

	$easing = get_field('slider_easing_name','option');
}else{

	$easing = '';
}

if(get_field('slider_arrows_name','option') ) {

	$directionNav = true;
}else{

	$directionNav = false;
}

$prevText = get_field('slider_navigation_prev_name','option');
$nextText = get_field('slider_navigation_next_name','option');

if( get_field( 'slider_controlnav_name','option' ) == 'none' ) {

	$controlNav = false;
}elseif( get_field( 'slider_controlnav_name','option' ) == 'bullets' ) {

	$controlNav = 'true';
}elseif( get_field( 'slider_controlnav_name','option' ) == 'thumbnails' ) {

	$controlNav = 'thumbnails';
}else{

	$controlNav = false;
}

if( get_field( 'slider_thumbnails_fit_name','option' ) && get_field( 'slider_controlnav_name','option' ) == 'thumbnails' ) {

	$fit = 'fit-thumbnails';
}else{

	$fit = '';
}

if(get_field('slider_loop_name','option') ) {

	$animationLoop = true;
}else{

	$animationLoop = false;
}

if(get_field('slider_direction_name','option') ) {

	$direction = get_field('slider_direction_name','option');
}else{

	$direction = '';
}

if(get_field('slider_randomize_name','option') ) {

	$randomize = true;
}else{

	$randomize = false;
}

if(get_field('slider_speed_name','option') ) {

	$slideshowSpeed = get_field('slider_speed_name','option');
}else{

	$slideshowSpeed = '';
}

if(get_field('slider_animationspeed_name','option') ) {

	$animationSpeed = get_field('slider_animationspeed_name','option');
}else{

	$animationSpeed = '';
}

if(get_field('slider_pause_name','option') ) {

	$pauseOnHover = true;
}else{

	$pauseOnHover = false;
}

if(get_field('slider_reverse_name','option') ) {

	$reverse = true;
}else{

	$reverse = false;
}

if(get_field('slider_usecss_name','option') ) {

	$useCSS = true;
}else{

	$useCSS = false;
}

if(get_field('slider_touch_name','option') ) {

	$touch = true;
}else{

	$touch = false;
}

if(get_field('slider_keyboard_name','option') ) {

	$keyboard = true;
}else{

	$keyboard = false;
}

if(get_field('slider_ie_fade_name','option') ) {

	$ie_fade = true;
}else{

	$ie_fade = false;
}

$controlsContainer = get_field( 'slider_customcontrols_name','options' );
$customDirectionNav = get_field( 'slider_customdirectionnav_name','option' );
$customManualcontrols = get_field( 'slider_manualcontrols_name','option' );

$data = array(
	'slider' => array(
		'animation'				=> (string) $animation,
		'easing'				=> (string) $easing,
		'directionNav'			=> (bool) $directionNav,
		'prevText'				=> (string) $prevText,
		'nextText'				=> (string) $nextText,
		'controlNav'			=> (string) $controlNav,
		'animationLoop'			=> (bool) $animationLoop,
		'direction'				=> (string) $direction,
		'randomize'				=> (bool) $randomize,
		'slideshowSpeed'		=> (int) $slideshowSpeed,
		'animationSpeed'		=> (int) $animationSpeed,
		'pauseOnHover'			=> (bool) $pauseOnHover,
		'reverse'				=> (bool) $reverse,
		'useCSS'				=> (bool) $useCSS,
		'touch'					=> (bool) $touch,
		'keyboard'				=> (bool) $keyboard,
		'ieFade'				=> (bool) $ie_fade,
		'controlsContainer'		=> (string) $controlsContainer,
		'customDirectionNav'	=> (string) $customDirectionNav,
		'customManualcontrols'	=> (string) $customManualcontrols,
	),
);