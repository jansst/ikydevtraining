<?php

/**
* CIS-165PH  Asn 6
* maps.php
* Purpose: Google maps module core
*
* @author Wilco Verhaar
* @version 2.0 8/12/17
*/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if( ! function_exists( 'min_single_maps_files' ) ) {

	/**
	 * Registers needed scripts and styles for the single map version
	 * @return type
	 */

	function min_single_maps_files() {

		// Get api key
		$api_key = get_field( 'setting_maps_module_api_name', 'option' );

		// Register api key
		wp_register_script( 'min_maps_single_api_script', 'https://maps.googleapis.com/maps/api/js?key='.$api_key, null, null, true );

		// Register local script
		wp_register_script( 'min_maps_single_local_script', MIN_MAP_URI. 'single/js/local.js' );

		// Register theme map style
		wp_register_style( 'min_maps_single_style', MIN_MAP_URI. 'single/css/maps.css' );
	}
}
add_action( 'wp_enqueue_scripts', 'min_single_maps_files' );

if ( ! function_exists( 'min_maps_single_post_type' ) ) {

	/**
	 * Creates the post type
	 * @return type
	 */

	function min_maps_single_post_type() {

		// Create labels
		$labels = array(
			'name'                  => _x( 'Maps', 'minimal210' ),
			'singular_name'         => _x( 'Map', 'minimal210' ),
			'menu_name'             => __( 'Maps', 'minimal210' ),
			'name_admin_bar'        => __( 'Maps', 'minimal210' ),
			'archives'              => __( 'Maps Archives', 'minimal210' ),
			'attributes'            => __( 'Maps Attributes', 'minimal210' ),
			'parent_item_colon'     => __( 'Parent map:', 'minimal210' ),
			'all_items'             => __( 'Maps', 'minimal210' ),
			'add_new_item'          => __( 'Add New map', 'minimal210' ),
			'add_new'               => __( 'Add map', 'minimal210' ),
			'new_item'              => __( 'New map', 'minimal210' ),
			'edit_item'             => __( 'Edit map', 'minimal210' ),
			'update_item'           => __( 'Update map', 'minimal210' ),
			'view_item'             => __( 'View map', 'minimal210' ),
			'view_items'            => __( 'View maps', 'minimal210' ),
			'search_items'          => __( 'Search maps', 'minimal210' ),
			'not_found'             => __( 'Not found', 'minimal210' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'minimal210' ),
			'insert_into_item'      => __( 'Insert into map', 'minimal210' ),
			'uploaded_to_this_item' => __( 'Uploaded to this map', 'minimal210' ),
			'items_list'            => __( 'Maps list', 'minimal210' ),
			'items_list_navigation' => __( 'Maps list navigation', 'minimal210' ),
			'filter_items_list'     => __( 'Filter maps list', 'minimal210' ),
		);

		// Create capabilities
		$capabilities = array(

			'publish_posts' 		=> 'manage_options',
			'edit_posts' 			=> 'manage_options',
			'edit_others_posts' 	=> 'manage_options',
			'delete_posts' 			=> 'manage_options',
			'delete_others_posts' 	=> 'manage_options',
			'read_private_posts' 	=> 'manage_options',
			'edit_post' 			=> 'manage_options',
			'delete_post' 			=> 'manage_options',
			'read_post' 			=> 'manage_options',
		);

		// Apply filters
		$labels = apply_filters( 'min/maps/single/labels', $labels );
		$show_in_menu = apply_filters( 'min/maps/single/menu_location', 'options-general.php' );
		$capabilities = apply_filters( 'min/maps/single/capabilities', $capabilities );

		$args = array(
			'label'                 => __( 'Map', 'minimal210' ),
			'labels'                => $labels,
			'supports'              => array( 'title' ),
			'hierarchical'          => false,
			'public'                => false,
			'show_ui'               => true,
			'show_in_menu'          => $show_in_menu,
			'show_in_admin_bar'     => false,
			'show_in_nav_menus'     => false,
			'can_export'            => true,
			'has_archive'           => false,
			'rewrite'				=> false,
			'exclude_from_search'   => true,
			'publicly_queryable'    => true,
			'capability_type'       => 'post',
			'capabilities' 			=> $capabilities,
		);

		$args = apply_filters( 'min/maps/single/posttype_args', $args );
		
		register_post_type( 'min_maps_single', $args );
	}
	add_action( 'init', 'min_maps_single_post_type', 0 );
}

/*-------------------------------------------------------------------------------
	ACF group
-------------------------------------------------------------------------------*/

require_once( MIN_MAP_PATH. 'single/settings.php' );

/*-------------------------------------------------------------------------------
	Create Shortcode
-------------------------------------------------------------------------------*/

if( ! function_exists( 'min_maps_single_shortcode' ) ) {

	/**
	 * Creates the shortcode min-map
	 * @param type $atts 
	 * @return type
	 */

	function min_maps_single_shortcode( $atts ) {

		// If the module is activated
		$module_active = get_field( 'field_setting_maps_module_key', 'option' );

		if( is_404() || is_search() || $module_active == false )
			return;

		// Get post
		global $post;
		$post_id = $post->ID;

		$atts = shortcode_atts( array( 0 => '' ), $atts);
		$map_id = $atts[0];

		if( min_post_exists( $map_id ) ) {

			// If the post comes from the right post type
			if( get_post_type( $map_id ) == 'min_maps_single' ) {

				$enqueue_enabled 	= get_field( 'setting_maps_load_api_key', 'option' );
				$api_key 			= get_field( 'setting_maps_module_api_name', 'option' );

				// If Enqueue setting is enabled
				if( $enqueue_enabled == true ) {

					// If API key exists
					if( ! empty( $api_key ) ) {

						// Register API key
						wp_enqueue_script( 'min_maps_single_api_script' );
					
					}elseif( is_user_logged_in() ) {

						// NEEDS WORK (sprint)
						return __( 'No valid api key. Insert a key <a target="_blank" href="/wp-admin/options-general.php?page=settings-minimal210">here</a>', 'minimal210' ); 
					}
				}
				
				// Get data
				$data_object = min_maps_single_get_local_settings( $map_id, $post_id );

				// If there was an error
				if( array_key_exists( 'error', $data_object ) ) {

					// If the user is logged in
					// If the current user can administrator
					// If the array contains the key: error_text
					if( min_user_has_role( 'administrator' ) && array_key_exists( 'error_text', $data_object ) ) {

						// Give error
						echo $data_object['error_text'];
					}

					return;
				}

				// Load styles
				wp_enqueue_style( 'min_maps_single_style' );

				// Localize local script
				wp_localize_script( 'min_maps_single_local_script', 'min_maps_single_data', $data_object );

				// Load local script
				wp_enqueue_script( 'min_maps_single_local_script' );

				// Get location as array
				$location_array = min_maps_single_get_map_location( $map_id, $post_id );

				// If there was an error
				if( array_key_exists( 'error', $location_array ) ) {

					// If the user is logged in
					// If the current user can administrator
					// If the array contains the key: error_text
					if( min_user_has_role( 'administrator' ) && array_key_exists( 'error_text', $location_array ) ) {

						// Give error
						echo $location_array['error_text'];
					}

					return;
				}

				// Get marker image as array
				$marker_image_array = min_maps_single_get_marker_image( $map_id, $post_id );

				// If there was an error
				if( array_key_exists( 'error', $marker_image_array ) ) {

					// If the user is logged in
					// If the current user can administrator
					// If the array contains the key: error_text
					if( min_user_has_role( 'administrator' ) && array_key_exists( 'error_text', $marker_image_array ) ) {

						// Give error
						echo $marker_image_array['error_text'];
					}

					return;
				}

				// Get marker image
				$marker_image = $marker_image_array['content'];

				// Get location from array
				$location = $location_array['location'];

				// Load template
				include( locate_template( '/map-templates/content-map.php' ) );
			
			}else{

				// Wrong post type
			}

		// Not an existing post
		}elseif( min_user_has_role( 'administrator' ) ) {

			return __( 'The shortcode does not contain a numerical map id!','minimal210' );
		}
	}
	add_shortcode( 'min-map', 'min_maps_single_shortcode' );
}

if( ! function_exists( 'min_maps_single_get_local_settings' ) ) {

	/**
	 * Gets settings to be passed to javascript
	 * @param type $map_id 
	 * @param type $post_id 
	 * @return type
	 */

	function min_maps_single_get_local_settings( $map_id, $post_id ) {

		$data_array = array(

			'zoom_level' 			=> min_maps_single_get_zoom_level( $map_id, $post_id ),
		);

		$data = array();

		foreach( $data_array as $key => $value ) {

			if( is_array( $value ) ) {

				if( array_key_exists( 'error', $value ) == true ) {

					$data['error'] = true;
					$data['error_text'] = $value['error_text'];
				
				}else{

					$content = $value['content'];

					if( array_key_exists( 'type', $value ) ) {

						$type = $value['type'];
						settype( $content, $type );
					}

					$data['map_data'][$key] = $content;
				}
			}
		}

		return $data;
	}
}

if( ! function_exists( 'min_maps_single_get_zoom_level' ) ) {

	/**
	 * Get zoom level
	 * @param type $map_id 
	 * @param type $post_id 
	 * @return type
	 */

	function min_maps_single_get_zoom_level( $map_id, $post_id = '' ) {

		$zoom_level = array();
		$zoom_level['content'] = get_field( 'zoom_level', $map_id );
		$zoom_level['type'] = 'int';

		if( empty( $zoom_level['content'] ) )
			$zoom_level = 15;
		
		return $zoom_level;
	}
}

if( ! function_exists( 'min_maps_single_get_marker_image' ) ) {

	/**
	 * Get marker image
	 * @param type $map_id 
	 * @param type $post_id 
	 * @return type
	 */

	function min_maps_single_get_marker_image( $map_id, $post_id ) {

		// Create empty array
		$marker_image_array = array();

		// Get marker location
		$marker_location = get_field( 'marker_location', $map_id );

		// If marker location is on post
		if( $marker_location == true ) {

			$acf_location_name = get_field( 'marker_location_acf_name', $map_id );

			if( ! empty( $acf_location_name ) ) {

				if( min_map_get_field( $acf_location_name, $post_id ) ) {

					// Get field object
					$field_object = min_map_get_field_object( $acf_location_name, $post_id );

					// Get field object type
					$field_object_type = $field_object['type'];

					// Get field object format
					$field_object_format = $field_object['return_format'];

					// If type is image
					if( $field_object_type == 'image' ) {

						// If format is url
						if( $field_object_format == 'url' ) {

							// Add url to content
							$marker_image_array['content'] = min_map_get_field( $acf_location_name, $post_id );
						
						// If format is array
						}elseif( $field_object_format == 'array' ) {

							// Add url to content
							$marker_image_array['content'] = min_map_get_field( $acf_location_name, $post_id )['url'];
						
						// If format is id
						}elseif( $field_object_format == 'id' ) {

							// Add url to content
							$marker_image_array['content'] = wp_get_attachment_url( min_map_get_field( $acf_location_name, $post_id ) );
						}

					// Not a valid field type
					}else{

						$marker_image_array['error'] = true;
						$marker_image_array['error_text'] = __( 'Het veld: '.$acf_location_name.' is geen afbeeldingsveld' );
					}
				}
			
			}else{

				$marker_image_array['error'] = true;
				$marker_image_array['error_text'] = __( 'Geen locatie naam voor de marker image' );
			}
		
		}else{

			$marker_image_array['content'] = get_field( 'marker_image', $map_id );
		}

		return $marker_image_array;
	}
}

if( ! function_exists( 'min_maps_single_get_map_location' ) ) {

	/**
	 * Get map location
	 * @param type $map_id 
	 * @param type $post_id 
	 * @return type
	 */

	function min_maps_single_get_map_location( $map_id, $post_id ) {

		// Create empty array
		$location_array = array( 'location' => '' );

		// Get map location
		$map_location = get_field( 'map_location', $map_id );

		// If map location is on post
		if( $map_location == true ) {

			// Get location name
			$acf_location_name = get_field( 'acf_location_name', $map_id );

			// If location name is not empty
			if( ! empty( $acf_location_name ) ) {

				// If there is an value on field or subfield
				if( min_map_get_field( $acf_location_name, $post_id ) ) {

					// Get field object
					$field_object = min_map_get_field_object( $acf_location_name, $post_id );
					$field_object_type = $field_object['type'];

					if( $field_object_type == 'google_map' ) {

						$location_array['location'] = min_map_get_field( $acf_location_name, $post_id );
					
					}else{

						$marker_image_array['error'] = true;
						$marker_image_array['error_text'] = __( 'Het veld: '.$acf_location_name.' is geen Google maps veld' );
					}

				// If there is no location
				}else{

					// Set error true
					$location_array['error'] 		= true;

					// Set error text
					$location_array['error_text'] 	= __( 'Geen locatie' );
				}
			
			}else{

				// Clear location from post
				$location_array['error'] 		= true;

				// NEEDS WORK ( edit post links )
				$location_array['error_text'] = __( 'Geen locatie naam', 'minimal210' );
			}
		
		}else{

			$location_array['location'] = get_field( 'acf_map', $map_id );
		}

		if( empty( $location_array['location'] ) && array_key_exists( 'error', $location_array ) == false ) {

			$location_array['error']	= true;
			$location_array['error_text'] = __( 'Geen locatie' );
		}

		return $location_array;
	}
}

// Helper functions

if( ! function_exists( 'min_map_get_field' ) ) {

	/**
	 * Gets value from field or subfield
	 * @param type $fieldname 
	 * @param type|string $post_id 
	 * @return type
	 */

	function min_map_get_field( $fieldname, $post_id = '' ) {

		if( ! empty( get_field( $fieldname, $post_id ) ) ) {

			return get_field( $fieldname, $post_id );
		
		}elseif( ! empty( get_sub_field( $fieldname, $post_id ) ) ) {

			return get_sub_field( $fieldname, $post_id );
		}

		return;
	}
}

if( ! function_exists( 'min_map_get_field_object' ) ) {

	/**
	 * Gets value from field object or sub field object
	 * @param type $fieldname 
	 * @param type|string $post_id 
	 * @return type
	 */

	function min_map_get_field_object( $fieldname, $post_id = '' ) {

		if( ! empty( get_field_object( $fieldname, $post_id ) ) ) {

			return get_field_object( $fieldname, $post_id );
		
		}elseif( ! empty( get_sub_field_object( $fieldname, $post_id ) ) ) {

			return get_sub_field_object( $fieldname, $post_id );
		}

		return;
	}
}

// Admin functions

if( ! function_exists( 'min_maps_single_load_api_back' ) ) {

	/**
	 * Load api in backend
	 * @return type
	 */

	function min_maps_single_load_api_back() {

		$key = get_field( 'setting_maps_module_api_name','option' );

		if( $key ) {

			acf_update_setting( 'google_api_key', $key );
		}
	}
}
add_action( 'acf/init', 'min_maps_single_load_api_back' );

if( ! function_exists( 'min_maps_single_load_api_front' ) ) {

	/**
	 * Load api in front end 
	 * Technically usseles
	 * @param type $api 
	 * @return type
	 */

	function min_maps_single_load_api_front( $api ){

		if( get_field( 'field_setting_maps_module_api_admin_load_name','option' ) && get_field( 'setting_maps_module_api_name','option' ) ) {

			$key = get_field( 'setting_maps_module_api_name','option' );
			$api['key'] = $key;

		}else{

			$api['key'] = '';
		}

		return $api;
	}
}
add_filter('acf/fields/google_map/api', 'min_maps_single_load_api_front');

if( ! function_exists( 'min_modify_single_maps_columns' ) ) {

	/**
	 * Modifies the post type min_maps_single comlumns
	 * @param type $column 
	 * @return type
	 */

	function min_modify_single_maps_columns( $column ) {

		$column = array(
			"cb"                => "<input type=\"checkbox\" />",
			'title'             => 'Titel',
			'shortcode'			=> 'Shortcode',
		);

		return $column;
	}
}
add_filter( 'manage_min_maps_single_posts_columns','min_modify_single_maps_columns' );


if( ! function_exists( 'min_modify_single_maps_columns_content' ) ) {

	/**
	 * Adds content to the custom admin columns of the maps table
	 * @param type $column 
	 * @param type $post_id 
	 * @return type
	 */

	function min_modify_single_maps_columns_content( $column, $post_id ) {

		$columns_content = array(

			'shortcode'    => array(

				'field_value'	=> '<pre><input onclick="this.focus();this.select()" value="[min-map '.$post_id.']"></pre>',
				'no_url'		=> true,
			),
		);

		echo min_admin_custom_columns_content( $column, $post_id, $columns_content );
	}
}
add_action( 'manage_min_maps_single_posts_custom_column','min_modify_single_maps_columns_content', 10, 2 );

if( ! function_exists( 'min_remove_maps_single_row_actions' ) ) {

	/**
	 * Edit the wp admin row actions for abc_series
	 * @param type $actions 
	 * @param type $post 
	 * @return type
	 */

	function min_remove_maps_single_row_actions( $actions, $post ){

		if( $post->post_type == 'min_maps_single' ){

			// unset( $actions['edit'] );
			unset( $actions['inline hide-if-no-js'] );
			// unset( $actions['trash'] );
			unset( $actions['view'] );
		}
		return $actions;
	}
}
add_filter( 'post_row_actions', 'min_remove_maps_single_row_actions', 10, 2 );

function min_alter_side_shortcode_message_field( $field ) {

	$field['message'] = '';

	if( isset( $_GET['post'] ) ) {

		$post_id = $_GET['post'];

		$field['message'] = '<pre><input onclick="this.focus();this.select()" value="[min-map '.$post_id.']"></pre>';
	}

	return $field;
}
add_filter('acf/load_field/key=field_side_shortcode', 'min_alter_side_shortcode_message_field');
