<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/*-------------------------------------------------------------------------------
	Enqueue scripts and styles
-------------------------------------------------------------------------------*/

if( ! function_exists('minimal_carousel_scripts') ) {

	function minimal_carousel_scripts() {

		// Create path
		$path = get_template_directory_uri(). '/inc/carousel';

		// Frontend styles
		wp_register_style('minimal-carousel-style', $path. '/css/owl.carousel.min.css');
		wp_register_style('minimal-carousel-theme-style', $path. '/css/theme-carousel.css');

		// Flexslider library
		wp_register_script('minimal-carousel-library', $path. '/js/owl.carousel.min.js', array('jquery') );
		wp_register_script('minimal-carousel-local', $path. '/js/local.js', array('jquery') );

	}
	add_action( 'wp_enqueue_scripts', 'minimal_carousel_scripts' );
}

/*-------------------------------------------------------------------------------
	Enqueue admin scripts
-------------------------------------------------------------------------------*/

if( ! function_exists('minimal_carousel_adminscripts') ) {

	function minimal_carousel_adminscripts() {


	}
	// add_action( 'admin_enqueue_scripts', 'minimal_carousel_adminscripts' );
}

/*-------------------------------------------------------------------------------
	Carousel post type
-------------------------------------------------------------------------------*/

if ( ! function_exists( 'minimal_carousel_post_type' ) ) {

	function minimal_carousel_post_type() {

		$labels = array(
			'name'                  => _x( 'Carousel', 'minimal210' ),
			'singular_name'         => _x( 'Carousel', 'minimal210' ),
			'menu_name'             => __( 'Carousel', 'minimal210' ),
			'name_admin_bar'        => __( 'Carousel', 'minimal210' ),
			'archives'              => __( 'Carousel Archives', 'minimal210' ),
			'attributes'            => __( 'Carousel Attributes', 'minimal210' ),
			'parent_item_colon'     => __( 'Parent carousel:', 'minimal210' ),
			'all_items'             => __( 'Carousel', 'minimal210' ),
			'add_new_item'          => __( 'Add New carousel', 'minimal210' ),
			'add_new'               => __( 'Add carousel', 'minimal210' ),
			'new_item'              => __( 'New carousel', 'minimal210' ),
			'edit_item'             => __( 'Edit carousel', 'minimal210' ),
			'update_item'           => __( 'Update carousel', 'minimal210' ),
			'view_item'             => __( 'View carousel', 'minimal210' ),
			'view_items'            => __( 'View carousels', 'minimal210' ),
			'search_items'          => __( 'Search carousels', 'minimal210' ),
			'not_found'             => __( 'Not found', 'minimal210' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'minimal210' ),
			'insert_into_item'      => __( 'Insert into carousel', 'minimal210' ),
			'uploaded_to_this_item' => __( 'Uploaded to this carousel', 'minimal210' ),
			'items_list'            => __( 'Carousels list', 'minimal210' ),
			'items_list_navigation' => __( 'Carousels list navigation', 'minimal210' ),
			'filter_items_list'     => __( 'Filter carousels list', 'minimal210' ),
		);
		$args = array(
			'label'                 => __( 'Carousel', 'minimal210' ),
			'labels'                => $labels,
			'supports'              => array( 'title', 'author' ),
			'hierarchical'          => false,
			'public'                => false,
			'show_ui'               => true,
			'show_in_menu'          => 'options-general.php',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => false,
			'can_export'            => true,
			'has_archive'           => false,	
			'exclude_from_search'   => true,
			'publicly_queryable'    => true,
			'capability_type'       => 'post',
		);
		register_post_type( 'carousel', $args );
	}
	add_action( 'init', 'minimal_carousel_post_type', 0 );
}

/*-------------------------------------------------------------------------------
	ACF group
-------------------------------------------------------------------------------*/

require(get_template_directory() .'/inc/carousel/settings.php');

/*-------------------------------------------------------------------------------
	Create Shortcode
-------------------------------------------------------------------------------*/

if( ! function_exists( 'minimal_carousel_shortcode' ) ) {

	function minimal_carousel_shortcode( $atts ) {

		$atts = shortcode_atts( array(
			0 => '',
		), $atts);

		if( ctype_digit( $atts[0] ) ) {

			$get_carousel = array(
				'post_type' 			=> 'carousel',
				'posts_per_page'		=> '1',
				'post__in'				=> array( $atts[0] ),
			);

			$the_query = new WP_Query( $get_carousel );

			if( $the_query->have_posts() ) { 

				while ( $the_query->have_posts() ) { $the_query->the_post();

					require( get_template_directory() .'/inc/carousel/get-fields.php');

					$get_carousel_posts = array(

						'post_type'			=> $post_type,
						'tax_query' 		=> $the_taxonomies,
						'posts_per_page'	=> -1,
					);

					$the_carousel_query = new WP_Query( $get_carousel_posts );

					if( $the_carousel_query->have_posts() ) { 

						wp_enqueue_style('minimal-carousel-style');

						wp_enqueue_style('minimal-carousel-theme-style');

						wp_enqueue_script('minimal-carousel-library');

						wp_enqueue_script('minimal-carousel-local');

						wp_localize_script('minimal-carousel-local', 'carousel', $data);

						?>

						<div class="owl-carousel carousel-<?php echo $atts[0]; ?>">

							<?php while( $the_carousel_query->have_posts() ) { $the_carousel_query->the_post();

								get_template_part( '/carousel-templates/content','carousel' );

							} ?>

						</div>

						<?php
						wp_reset_postdata();
					}
				}
				wp_reset_postdata();
			}

		}else{ // Digit

			if( is_user_logged_in() ) {

				echo __( 'The shortcode does not contain a numerical carousel id','minimal210' );
			} 
		} // Digit 
	}
	add_shortcode( 'carousel', 'minimal_carousel_shortcode' );
}

/*-------------------------------------------------------------------------------
	Create Columns
-------------------------------------------------------------------------------*/

function minimal_carousel_shortcode_column( $columns ) {

	$new = array();

	foreach( $columns as $key => $title ) {

		if ( $key=='author' ) {
			$new['shortcode'] = 'Shortcode';
		}

		$new[$key] = $title;
	}
	return $new;
}

/*-------------------------------------------------------------------------------
	Create columns content
-------------------------------------------------------------------------------*/

function minimal_carousel_shortcode_column_content( $column ) {
  
	global $post;
  
	if( $column == 'shortcode' ) {

		  echo '<pre><input onclick="this.focus();this.select()" value="[carousel '.get_the_id().']"></pre>';
	}
}
add_filter( 'manage_carousel_posts_columns' , 'minimal_carousel_shortcode_column' );
add_action( 'manage_carousel_posts_custom_column', 'minimal_carousel_shortcode_column_content' );