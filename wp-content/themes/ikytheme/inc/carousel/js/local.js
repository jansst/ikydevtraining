jQuery( document ).ready( function() {

	console.log(carousel.carousel.class);

	jQuery(carousel.carousel.class).owlCarousel({

		items				: carousel.carousel.items,
		margin				: carousel.carousel.margin,
		loop				: carousel.carousel.loop,
		nav					: carousel.carousel.nav,
		navText				: carousel.carousel.navText,
		dots				: carousel.carousel.dots,
		autoplay			: carousel.carousel.autoplay,

		// Advanced
		center				: carousel.carousel.center,
		mouseDrag			: carousel.carousel.mouseDrag,
		touchDrag			: carousel.carousel.touchDrag,
		pullDrag			: carousel.carousel.pullDrag,
		freeDrag			: carousel.carousel.freeDrag,
		stagePadding		: carousel.carousel.stagePadding,
		autoWidth			: carousel.carousel.autoWidth,
		merge				: carousel.carousel.merge,
		mergeFit			: carousel.carousel.mergeFit,
		startPosition 		: carousel.carousel.startPosition,
		URLhashListener		: carousel.carousel.URLhashListener,
		autoplayHoverPause	: carousel.carousel.autoplayHoverPause,
	});

});