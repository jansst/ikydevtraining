<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if( ! function_exists( 'minimal_carousel_local_field_group' ) ) {

	function minimal_carousel_local_field_group() {

		acf_add_local_field_group( array (
	        'key'      => 'carousel_post_group',
	        'title'    => __( 'Carousel settings','minimal210' ),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'carousel',
					),
				),
			),
	        'menu_order'            => 0,
	        'position'              => 'normal',
	        'style'                 => 'default',
	        'label_placement'       => 'top',
	        'instruction_placement' => 'label',
	        'hide_on_screen'        => '',
	        'active'				=> 1,
	        'description'			=> '',
	    ) );

		acf_add_local_field( array(
	        'key'          		=> 'carousel_tab_general',
	        'label'        		=> __( 'General','minimal210' ),
	        'name'         		=> '',
	        'type'         		=> 'tab',
	        'endpoint'			=> 0,
	        'parent'       		=> 'carousel_post_group',
   		) );

   		acf_add_local_field( array(
	        'key'          		=> 'carousel_setting_posttype_key',
	        'label'        		=> __( 'Post type','minimal210' ),
	        'name'         		=> 'carousel_setting_posttype_name',
	        'parent'       		=> 'carousel_post_group',
	        'type'         		=> 'posttype_select',
	        'multiple'			=> 1,
	        'default_value' 	=> '',
   		) );

		$args = array( 'public' => true, ); 
		$output = 'objects';
   		$taxonomies = get_taxonomies( $args,$output ); 

   		if( ! empty( $taxonomies ) ) {

   			foreach( $taxonomies as $taxonomy ) {

				$terms = get_terms( array(
					'taxonomy' => $taxonomy->name,
					'hide_empty' => false,
				) );

				if( ! empty($terms) ) { 

			   		acf_add_local_field( array(
				        'key'          		=> 'carousel_setting_taxonomy_'.$taxonomy->name.'_key',
				        'label'        		=> __( $taxonomy->label,'minimal210' ),
				        'name'         		=> 'carousel_setting_taxonomy_'.$taxonomy->name.'_name',
				        'parent'       		=> 'carousel_post_group',
				        'type'         		=> 'taxonomy',
				        'taxonomy' 			=> $taxonomy->name,
				        'field_type' 		=> 'checkbox',
				        'multiple'			=> 1,
				        'default_value' 	=> '',
				        'return_format'		=> 'id',
				        'add_term'			=> 0,
				        'save_terms'		=> 0,
				        'load_terms'		=> 0,
				        'wrapper' 			=> array (
							'width' 		=> '100',
							'class' 		=> '',
							'id' 			=> '',
						),
			   		) );
			   	}
	   		}
   		}
		
		// Items
		acf_add_local_field( array(
	        'key'          		=> 'carousel_setting_items_key',
	        'label'        		=> __( 'Items','minimal210' ),
	        'name'         		=> 'carousel_setting_items_name',
	        'parent'       		=> 'carousel_post_group',
	        'instructions'		=> __( 'The number of items you want to see on the screen.','minimal210' ),
	        'type'         		=> 'number',
	        'min'				=> '1',
	        'step'				=> '1',
	        'wrapper' 			=> array (
									'width' => '50',
									'class' => '',
									'id' => '',
								),
   		) );

   		acf_add_local_field( array(
	        'key'          		=> 'carousel_setting_margin_key',
	        'label'        		=> __( 'Margin','minimal210' ),
	        'name'         		=> 'carousel_setting_margin_name',
	        'parent'       		=> 'carousel_post_group',
	        'instructions'		=> __( 'margin-right(px) on item.','minimal210' ),
	        'type'         		=> 'number',
	        'default_value'		=> '0',
	        'step'				=> '10',
	        'wrapper' 			=> array (
									'width' => '50',
									'class' => '',
									'id' => '',
								),
   		) );

   		acf_add_local_field( array(
	        'key'          		=> 'carousel_setting_loop_key',
	        'label'        		=> __( 'Loop','minimal210' ),
	        'name'         		=> 'carousel_setting_loop_name',
	        'parent'       		=> 'carousel_post_group',
	        'instructions'		=> __( 'Infinity loop. Duplicate last and first items to get loop illusion.','minimal210' ),
	        'type'         		=> 'checkbox',
	        'choices'			=> array( 'true' => 'Enable' ),
	        'default_value'		=> true,
	        'wrapper' 			=> array (
									'width' => '50',
									'class' => '',
									'id' => '',
								),
   		) );

   		acf_add_local_field( array(
	        'key'          		=> 'carousel_setting_nav_key',
	        'label'        		=> __( 'Next/Previous buttons','minimal210' ),
	        'name'         		=> 'carousel_setting_nav_name',
	        'parent'       		=> 'carousel_post_group',
	        'instructions'		=> __( 'Show next/prev buttons.','minimal210' ),
	        'type'         		=> 'checkbox',
	        'choices'			=> array( 'true' => 'Enable' ),
	        'default_value'		=> true,
	        'wrapper' 			=> array (
									'width' => '50',
									'class' => '',
									'id' => '',
								),
   		) );

   		acf_add_local_field( array(
	        'key'          		=> 'carousel_setting_navnext_key',
	        'label'        		=> __( 'Next text','minimal210' ),
	        'name'         		=> 'carousel_setting_navnext_name',
	        'parent'       		=> 'carousel_post_group',
	        'instructions'		=> __( 'Does not work if there are not enough items.','minimal210' ),
	        'type'         		=> 'text',
	        'default_value'		=> 'Next',
	        'wrapper' 			=> array (
									'width' => '50',
									'class' => '',
									'id' => '',
								),
   		) );

   		$value = get_field('carousel_setting_navnext_name');
		$value = htmlspecialchars($value, ENT_QUOTES);

   		acf_add_local_field( array(
	        'key'          		=> 'carousel_setting_navprev_key',
	        'label'        		=> __( 'Previous text','minimal210' ),
	        'name'         		=> 'carousel_setting_navprev_name',
	        'parent'       		=> 'carousel_post_group',
	        'instructions'		=> __( 'Does not work if there are not enough items.','minimal210' ),
	        'type'         		=> 'text',
	        'default_value'		=> 'Previous',
	        'wrapper' 			=> array (
									'width' => '50',
									'class' => '',
									'id' => '',
								),
   		) );

   		acf_add_local_field( array(
	        'key'          		=> 'carousel_setting_dots_key',
	        'label'        		=> __( 'Dots','minimal210' ),
	        'name'         		=> 'carousel_setting_dots_name',
	        'parent'       		=> 'carousel_post_group',
	        'instructions'		=> __( 'Show dots navigation..','minimal210' ),
	        'type'         		=> 'checkbox',
	        'choices'			=> array( 'true' => 'Enable' ),
	        'default_value'		=> false,
	        'wrapper' 			=> array (
									'width' => '50',
									'class' => '',
									'id' => '',
								),
   		) );

   		acf_add_local_field( array(
	        'key'          		=> 'carousel_setting_autoplay_key',
	        'label'        		=> __( 'Autoplay','minimal210' ),
	        'name'         		=> 'carousel_setting_autoplay_name',
	        'parent'       		=> 'carousel_post_group',
	        'instructions'		=> __( '','minimal210' ),
	        'type'         		=> 'checkbox',
	        'choices'			=> array( 'true' => 'Enable' ),
	        'default_value'		=> false,
	        'wrapper' 			=> array (
									'width' => '50',
									'class' => '',
									'id' => '',
								),
   		) );

 		// ADVANCED
   		acf_add_local_field( array(
	        'key'          		=> 'carousel_tab_advanced',
	        'label'        		=> __( 'Advanced','minimal210' ),
	        'name'         		=> '',
	        'type'         		=> 'tab',
	        'endpoint'			=> 0,
	        'parent'       		=> 'carousel_post_group',
   		) );

   		acf_add_local_field( array(
	        'key'          		=> 'carousel_setting_center_key',
	        'label'        		=> __( 'Center','minimal210' ),
	        'name'         		=> 'carousel_setting_center_name',
	        'parent'       		=> 'carousel_post_group',
	        'instructions'		=> __( 'Center item. Works well with even an odd number of items.','minimal210' ),
	        'type'         		=> 'checkbox',
	        'choices'			=> array( 'true' => 'Enable' ),
	        'default_value'		=> false,
	        'wrapper' 			=> array (
									'width' => '50',
									'class' => '',
									'id' => '',
								),
   		) );

   		acf_add_local_field( array(
	        'key'          		=> 'carousel_setting_mousedrag_key',
	        'label'        		=> __( 'Mouse drag','minimal210' ),
	        'name'         		=> 'carousel_setting_mousedrag_name',
	        'parent'       		=> 'carousel_post_group',
	        'instructions'		=> __( 'Mouse drag enabled.','minimal210' ),
	        'type'         		=> 'checkbox',
	        'choices'			=> array( 'true' => 'Enable' ),
	        'default_value'		=> true,
	        'wrapper' 			=> array (
									'width' => '50',
									'class' => '',
									'id' => '',
								),
   		) );

   		acf_add_local_field( array(
	        'key'          		=> 'carousel_setting_touchdrag_key',
	        'label'        		=> __( 'Touch drag','minimal210' ),
	        'name'         		=> 'carousel_setting_touchdrag_name',
	        'parent'       		=> 'carousel_post_group',
	        'instructions'		=> __( 'Touch drag enabled.','minimal210' ),
	        'type'         		=> 'checkbox',
	        'choices'			=> array( 'true' => 'Enable' ),
	        'default_value'		=> true,
	        'wrapper' 			=> array (
									'width' => '50',
									'class' => '',
									'id' => '',
								),
   		) );

   		acf_add_local_field( array(
	        'key'          		=> 'carousel_setting_pulldrag_key',
	        'label'        		=> __( 'Pull drag','minimal210' ),
	        'name'         		=> 'carousel_setting_pulldrag_name',
	        'parent'       		=> 'carousel_post_group',
	        'instructions'		=> __( 'Stage pull to edge.','minimal210' ),
	        'type'         		=> 'checkbox',
	        'choices'			=> array( 'true' => 'Enable' ),
	        'default_value'		=> true,
	        'wrapper' 			=> array (
									'width' => '50',
									'class' => '',
									'id' => '',
								),
   		) );

   		acf_add_local_field( array(
	        'key'          		=> 'carousel_setting_freedrag_key',
	        'label'        		=> __( 'Free drag','minimal210' ),
	        'name'         		=> 'carousel_setting_freedrag_name',
	        'parent'       		=> 'carousel_post_group',
	        'instructions'		=> __( 'Item pull to edge.','minimal210' ),
	        'type'         		=> 'checkbox',
	        'choices'			=> array( 'true' => 'Enable' ),
	        'default_value'		=> false,
	        'wrapper' 			=> array (
									'width' => '50',
									'class' => '',
									'id' => '',
								),
   		) );

   		acf_add_local_field( array(
	        'key'          		=> 'carousel_setting_stagepadding_key',
	        'label'        		=> __( 'Stage padding','minimal210' ),
	        'name'         		=> 'carousel_setting_stagepadding_name',
	        'parent'       		=> 'carousel_post_group',
	        'instructions'		=> __( 'Padding left and right on stage (can see neighbours).','minimal210' ),
	        'type'         		=> 'number',
	        'default_value'		=> '0',
	        'step'				=> '10',
	        'wrapper' 			=> array (
									'width' => '50',
									'class' => '',
									'id' => '',
								),
   		) );

   		acf_add_local_field( array(
	        'key'          		=> 'carousel_setting_autowidth_key',
	        'label'        		=> __( 'Auto width','minimal210' ),
	        'name'         		=> 'carousel_setting_autowidth_name',
	        'parent'       		=> 'carousel_post_group',
	        'instructions'		=> __( 'Set non grid content. Try using width style on divs.','minimal210' ),
	        'type'         		=> 'checkbox',
	        'choices'			=> array( 'true' => 'Enable' ),
	        'default_value'		=> false,
	        'wrapper' 			=> array (
									'width' => '50',
									'class' => '',
									'id' => '',
								),
   		) );

   		acf_add_local_field( array(
	        'key'          		=> 'carousel_setting_merge_key',
	        'label'        		=> __( 'Merge','minimal210' ),
	        'name'         		=> 'carousel_setting_merge_name',
	        'parent'       		=> 'carousel_post_group',
	        'instructions'		=> __( 'Merge items. Looking for data-merge=&#39;{number}&#39; inside item..','minimal210' ),
	        'type'         		=> 'checkbox',
	        'choices'			=> array( 'true' => 'Enable' ),
	        'default_value'		=> false,
	        'wrapper' 			=> array (
									'width' => '50',
									'class' => '',
									'id' => '',
								),
   		) );

   		acf_add_local_field( array(
	        'key'          		=> 'carousel_setting_mergefit_key',
	        'label'        		=> __( 'Merge fit','minimal210' ),
	        'name'         		=> 'carousel_setting_mergefit_name',
	        'parent'       		=> 'carousel_post_group',
	        'instructions'		=> __( 'Fit merged items if screen is smaller than items value.','minimal210' ),
	        'type'         		=> 'checkbox',
	        'choices'			=> array( 'true' => 'Enable' ),
	        'default_value'		=> true,
	        'wrapper' 			=> array (
									'width' => '50',
									'class' => '',
									'id' => '',
								),
   		) );

   		acf_add_local_field( array(
	        'key'          		=> 'carousel_setting_startposition_key',
	        'label'        		=> __( 'Start position','minimal210' ),
	        'name'         		=> 'carousel_setting_startposition_name',
	        'parent'       		=> 'carousel_post_group',
	        'instructions'		=> __( 'Start position or URL Hash string like &#39;#id&#39;.','minimal210' ),
	        'type'         		=> 'text',
	        'default_value'		=> '',
	        'wrapper' 			=> array (
									'width' => '50',
									'class' => '',
									'id' => '',
								),
   		) );

   		acf_add_local_field( array(
	        'key'          		=> 'carousel_setting_urlhashlistener_key',
	        'label'        		=> __( 'URL hash Listener','minimal210' ),
	        'name'         		=> 'carousel_setting_urlhashlistener_name',
	        'parent'       		=> 'carousel_post_group',
	        'instructions'		=> __( 'Listen to url hash changes. data-hash on items is required.','minimal210' ),
	        'type'         		=> 'checkbox',
	        'choices'			=> array( 'true' => 'Enable' ),
	        'default_value'		=> true,
	        'wrapper' 			=> array (
									'width' => '50',
									'class' => '',
									'id' => '',
								),
   		) );

   		acf_add_local_field( array(
	        'key'          		=> 'carousel_setting_autoplayhoverpause_key',
	        'label'        		=> __( 'Autoplay hover pause','minimal210' ),
	        'name'         		=> 'carousel_setting_autoplayhoverpause_name',
	        'parent'       		=> 'carousel_post_group',
	        'instructions'		=> __( 'Pause on mouse hover.','minimal210' ),
	        'type'         		=> 'checkbox',
	        'choices'			=> array( 'true' => 'Enable' ),
	        'default_value'		=> true,
	        'wrapper' 			=> array (
									'width' => '50',
									'class' => '',
									'id' => '',
								),
   		) );

 

	}
	add_action( 'init', 'minimal_carousel_local_field_group' );
}

?>