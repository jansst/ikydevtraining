<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/*-------------------------------------------------------------------------------
	Get post types
-------------------------------------------------------------------------------*/

if( get_field( 'carousel_setting_posttype_name' ) ) {

	$post_type = get_field( 'carousel_setting_posttype_name');
}else{

	$post_type = '';
}

/*-------------------------------------------------------------------------------
	Retrieve selected taxonomy terms
-------------------------------------------------------------------------------*/

$args = array( 'public' => true, ); 
$output = 'objects';
$get_taxonomies = get_taxonomies( $args,$output ); 

if( ! empty( $get_taxonomies ) ) {

	foreach( $get_taxonomies as $taxonomy ) {

		$terms = get_terms( array(
			'taxonomy' => $taxonomy->name,
			'hide_empty' => false,
		) );

		if( ! empty($terms) ) { 

			$selected_terms = get_field('carousel_setting_taxonomy_'.$taxonomy->name.'_name');

			if( $selected_terms ){

				foreach( $selected_terms as $id ){

					$the_taxonomies[] = array(
						'taxonomy' => $taxonomy->name,
						'field'    => 'term_id',
						'terms'    => $id,
					);

					$the_taxonomies['relation'] = 'OR';
				}
			}
   		}
	}
}

// Unique Class
$class = '.carousel-'.$atts[0];

// Items
// https://owlcarousel2.github.io/OwlCarousel2/docs/api-options.html#items
$items = get_field( 'carousel_setting_items_name' );

// Margin
// https://owlcarousel2.github.io/OwlCarousel2/docs/api-options.html#margin
$margin = get_field( 'carousel_setting_margin_name' );

// Loop
// https://owlcarousel2.github.io/OwlCarousel2/docs/api-options.html#loop
$loop = get_field( 'carousel_setting_loop_name' );

// Nav
// https://owlcarousel2.github.io/OwlCarousel2/docs/api-options.html#nav
$nav = get_field( 'carousel_setting_nav_name' );

// Nav Next
// https://owlcarousel2.github.io/OwlCarousel2/docs/api-options.html#navtext
$navNext = get_field( 'carousel_setting_navnext_name' );
$navPrev = get_field( 'carousel_setting_navprev_name' );
$navText = array();

if( !empty( $navNext ) && !empty( $navPrev ) ){

	$navText[] = $navNext;
	$navText[] = $navPrev;
}

// Dots
// https://owlcarousel2.github.io/OwlCarousel2/docs/api-options.html#dots
$dots = get_field( 'carousel_setting_dots_name' );

// Autoplay
// https://owlcarousel2.github.io/OwlCarousel2/docs/api-options.html#autoplay
$autoplay = get_field( 'carousel_setting_autoplay_name' );

// Center
// https://owlcarousel2.github.io/OwlCarousel2/docs/api-options.html#center
$center = get_field( 'carousel_setting_center_name' );

// MouseDrag
// https://owlcarousel2.github.io/OwlCarousel2/docs/api-options.html#mousedrag
$mouseDrag = get_field( 'carousel_setting_mousedrag_name' );

// TouchDrag
// https://owlcarousel2.github.io/OwlCarousel2/docs/api-options.html#touchdrag
$touchDrag = get_field( 'carousel_setting_touchdrag_name' );

// PullDrag
// https://owlcarousel2.github.io/OwlCarousel2/docs/api-options.html#pulldrag
$pullDrag = get_field( 'carousel_setting_pulldrag_name' );

// FreeDrag
// https://owlcarousel2.github.io/OwlCarousel2/docs/api-options.html#freedrag
$freeDrag = get_field( 'carousel_setting_freedrag_name' );

// Stagepadding
// https://owlcarousel2.github.io/OwlCarousel2/docs/api-options.html#stagepadding
$stagePadding = get_field( 'carousel_setting_stagepadding_name' );

// Auto width
// https://owlcarousel2.github.io/OwlCarousel2/docs/api-options.html#autowidth
$autoWidth = get_field( 'carousel_setting_autowidth_name' );

// Merge
// https://owlcarousel2.github.io/OwlCarousel2/docs/api-options.html#merge
$merge = get_field( 'carousel_setting_merge_name' );

// MergeFit
// https://owlcarousel2.github.io/OwlCarousel2/docs/api-options.html#mergefit
$mergeFit = get_field( 'carousel_setting_mergefit_name' );

// Start position
// https://owlcarousel2.github.io/OwlCarousel2/docs/api-options.html#startposition
$startPosition = get_field( 'carousel_setting_startposition_name' );

// URLhashListener
// https://owlcarousel2.github.io/OwlCarousel2/docs/api-options.html#urlhashlistener
$urlhash = get_field( 'carousel_setting_urlhashlistener_name' );

// Autoplay Hover pause
// https://owlcarousel2.github.io/OwlCarousel2/docs/api-options.html#autoplayhoverpause
$autoplayHoverPause = get_field( 'carousel_setting_autoplayhoverpause_name' );

// Data
// Use carousel.carousel.{name}
$data = array(
	'carousel' => array(
		'class'				=> (string) $class,
		'items'				=> (int) $items,
		'margin'			=> (int) $margin,
		'loop'				=> (bool) $loop,
		'nav'				=> (bool) $nav,
		'navText' 			=> (array) $navText,
		'dots' 				=> (bool) $dots,
		'autoplay' 			=> (bool) $autoplay,
		'center'			=> (bool) $center,
		'mouseDrag' 		=> (bool) $mouseDrag,
		'touchDrag' 		=> (bool) $touchDrag,
		'pullDrag'  		=> (bool) $pullDrag,
		'freeDrag'			=> (bool) $freeDrag,
		'stagePadding'		=> (int) $stagePadding,
		'autoWidth' 		=> (bool) $autoWidth,
		'merge'				=> (bool) $merge,
		'mergeFit'			=> (bool) $mergeFit,
		'startPosition' 	=> (string) $startPosition,
		'URLhashListener' 	=> (bool) $urlhash,
		'autoplayHoverPause'=> (bool) $autoplayHoverPause,
	),
);