<?php

/*-------------------------------------------------------------------------------
	After theme setup
-------------------------------------------------------------------------------*/

if ( ! function_exists( 'minimal210_setup' ) ) {

	function minimal210_setup() {

		// Load textdomain
		load_theme_textdomain( 'minimal210', get_template_directory() . '/languages' );

		// Add theme support
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'title-tag' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'customize-selective-refresh-widgets' );
		add_theme_support( 'custom-logo');
	    add_theme_support( 'woocommerce' );
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'minimal210' ),
		) );

		$GLOBALS['content_width'] = apply_filters( 'minimal210_content_width', 1080 );

		add_image_size( 'featured_preview', 400, 200 );
	}
	add_action( 'after_setup_theme', 'minimal210_setup' );
}

/*-------------------------------------------------------------------------------
	Theme requirements
-------------------------------------------------------------------------------*/

define( 'MIN_INC_PATH', get_template_directory(). '/inc/' );
define( 'MIN_MAP_PATH', get_template_directory(). '/inc/maps/' );
define( 'MIN_MAP_URI', get_template_directory_uri(). '/inc/maps/' );
define( 'MIN_RESPONSIVE_MENU_PATH', get_template_directory(). '/inc/responsive-menu/' );
define( 'MIN_RESPONSIVE_MENU_URI', get_template_directory_uri(). '/inc/responsive-menu/' );
define( 'MIN_CUSTOM_ACF_FIELDS_PATH', get_template_directory(). '/inc/settings/fields/' );
define( "INC_PATH", get_template_directory(). '/inc');

require( INC_PATH. '/customizer/customizer.php' );
require( INC_PATH. '/admin/admin.php' );
require( INC_PATH. '/settings/settings.php' );


/*-------------------------------------------------------------------------------
	Require modules on setting
-------------------------------------------------------------------------------*/

if( ! function_exists( 'min_require_theme_modules' ) ) {

    function min_require_theme_modules() {

        $slider_enabled             = get_field( 'setting_slider_module_name', 'option' );
        $carousel_enabled           = get_field( 'setting_carousel_module_name', 'option' );
        $maps_enabled               = get_field( 'setting_maps_module_name', 'option' );
        $breadcrumbs_enabled        = get_field( 'setting_activate_breadcrumbs', 'option' );
        $responsive_menu_enabled    = get_field( 'setting_activate_responsive_menu', 'option' );

        // Activated by default
        if( file_exists( INC_PATH. '/slider/slider.php' ) && $slider_enabled === NULL || $slider_enabled == true ) {

            require_once( INC_PATH. '/slider/slider.php' );
        }

        if( file_exists( INC_PATH. '/carousel/carousel.php' ) && $carousel_enabled == true ) {

            require_once( INC_PATH. '/carousel/carousel.php' );
        }

        if( file_exists( MIN_MAP_PATH. 'single/maps.php' ) && $maps_enabled == true ) {

            require_once( MIN_MAP_PATH. 'single/maps.php' );
        }

        if( file_exists( INC_PATH. '/breadcrumbs/breadcrumbs.php' ) && $breadcrumbs_enabled == true ) {

            require_once( INC_PATH. '/breadcrumbs/breadcrumbs.php' );
        }

        if( file_exists( MIN_RESPONSIVE_MENU_PATH. 'responsive-menu.php' ) && $responsive_menu_enabled == true ) {

            require_once( MIN_RESPONSIVE_MENU_PATH. 'responsive-menu.php' );
        }
    }
}
add_action( 'after_setup_theme', 'min_require_theme_modules' );

/*-------------------------------------------------------------------------------
	Thema updater
-------------------------------------------------------------------------------*/

// require 'inc/version/plugin-update-checker.php';
// $myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
// 	'http://minimal.buro210.nl/theme.json',
// 	__FILE__,
// 	'minimal210'
// );

/*-------------------------------------------------------------------------------
	Customize ACF path
-------------------------------------------------------------------------------*/

if( ! function_exists( 'minimal210_acf_settings_path' ) ) {

	function minimal210_acf_settings_path( $path ) {
	 
	    // update path
	    $path = get_template_directory() . '/inc/acf/';
	    
	    // return
	    return $path;
	}
	add_filter( 'acf/settings/path', 'minimal210_acf_settings_path' );
}

/*-------------------------------------------------------------------------------
	Customize ACF dir
-------------------------------------------------------------------------------*/

if( ! function_exists( 'minimal210_acf_settings_dir' ) ) {

	function minimal210_acf_settings_dir( $dir ) {
	 
		// update dir
		$dir = get_template_directory_uri() . '/inc/acf/';

		// return
		return $dir;
	}
	add_filter( 'acf/settings/dir', 'minimal210_acf_settings_dir' );
}

/*-------------------------------------------------------------------------------
	Include ACF
-------------------------------------------------------------------------------*/

include_once( get_template_directory() . '/inc/acf/acf.php' );


/*-------------------------------------------------------------------------------
	Enqueue scripts and styles
-------------------------------------------------------------------------------*/

if( ! function_exists( 'minimal210_scripts' ) ) {

	function minimal210_scripts() {

		wp_enqueue_script( 'jquery' );
		wp_enqueue_style( 'minimal210_style', get_stylesheet_uri() );
		wp_enqueue_script( 'minimal210_fontAwesome', 'https://use.fontawesome.com/a74136c82f.js' );

		// Frontend retrieved from settings page
		$frontend_path = get_template_directory_uri(). '/inc/frontend';

		if( get_field( 'setting_search_in_menu_name','option' ) ) {

			wp_enqueue_script( 'minimal210_search_in_menu_script', $frontend_path. '/js/search-in-menu.js', array( 'jquery' ) );
		}

		if( get_field( 'setting_sticky_header_name','option' ) ) {

			wp_enqueue_script( 'minimal210_sticky_menu_script', $frontend_path. '/js/sticky-menu.js', array( 'jquery' ) );
		}
	}
	add_action( 'wp_enqueue_scripts', 'minimal210_scripts' );
}









/*-------------------------------------------------------------------------------
	Enqueue scripts and styles
-------------------------------------------------------------------------------*/

if( ! function_exists( 'minimal210_scripts' ) ) {

	function minimal210_scripts() {

		wp_enqueue_script( 'jquery' );
		wp_enqueue_style( 'minimal210_style', get_stylesheet_uri() );
		wp_enqueue_script( 'minimal210_fontAwesome', 'https://use.fontawesome.com/a74136c82f.js' );

		// Frontend retrieved from settings page
		$frontend_path = get_template_directory_uri(). '/inc/frontend';

		if( get_field( 'setting_search_in_menu_name','option' ) ) {

			wp_enqueue_script( 'minimal210_search_in_menu_script', $frontend_path. '/js/search-in-menu.js', array( 'jquery' ) );
		}

		if( get_field( 'setting_sticky_header_name','option' ) ) {

			wp_enqueue_script( 'minimal210_sticky_menu_script', $frontend_path. '/js/sticky-menu.js', array( 'jquery' ) );
		}
	}
	add_action( 'wp_enqueue_scripts', 'minimal210_scripts' );
}






/*-------------------------------------------------------------------------------
	Menu
-------------------------------------------------------------------------------*/

if( ! function_exists( 'minimal210_menu' ) ) {

	function minimal210_menu() { ?>

		<nav id='main-navigation' class='block'>

			<?php wp_nav_menu( array( 'theme_location' => 'menu-1', 'menu_id' => 'primary-menu' ) ); ?>

		</nav>

	<?php }
}

/*-------------------------------------------------------------------------------
	Admin footer
-------------------------------------------------------------------------------*/

if( ! function_exists( 'minimal210_admin_footer' ) ) {

	function minimal210_admin_footer() {

		echo __( 'Created by IKY Creative','minimal210' );
	}
	add_filter( 'admin_footer_text', 'minimal210_admin_footer' );
}

/*-------------------------------------------------------------------------------
	Search in menu
-------------------------------------------------------------------------------*/

if( ! function_exists( 'minimal210_search_menu' ) ) {

	function minimal210_search_icon() {

		if( get_field( 'setting_search_in_menu_name','option' ) ) {

			echo '<div id="theme-search" class="block search-icon"><i class="fa fa-search" aria-hidden="true"></i></div>';

			get_search_form();
		}
	}
}

/*-------------------------------------------------------------------------------
	Numbered Pagination
-------------------------------------------------------------------------------*/

if( ! function_exists( 'minimal_pagination' ) ) {

    function minimal_pagination( $numpages = '', $pagerange = '', $paged='' ) {

        if( empty( $pagerange ) ) {
            $pagerange = 2;
        }

        global $paged;

        if( empty( $paged ) ) {
            $paged = 1;
        }
        if( $numpages == '' ) {
            global $wp_query;
            $numpages = $wp_query->max_num_pages;

            if( ! $numpages ) {
                $numpages = 1;
            }
        }

        if( get_option('permalink_structure') && !is_search() ) {

            $format = 'page/%#%/';

        }elseif(is_search()){

            $format = '&paged=%#%';
        }else{

            $format = '&paged=%#%';
        }   

        $pagination_args = array(
            'base'              => get_pagenum_link(1) . '%_%',
            'format'            => $format,
            'total'             => $numpages,
            'current'           => $paged,
            'show_all'          => False,
            'end_size'          => 1,
            'mid_size'          => $pagerange,
            'prev_next'         => True,
            'prev_text'         => __('Previous','minimal210'),
            'next_text'         => __('Next','minimal210'),
            'type'              => 'plain',
            'add_args'          => false,
            'add_fragment'      => ''
        );

        $paginate_links = paginate_links( $pagination_args );

        if( $paginate_links ) {
            echo '<nav class="minimal-pagination">';
            echo '<div class="page-of">Page ' . $paged . ' of ' . $numpages . '</div>';
            echo $paginate_links;
            echo '</nav>';
        }
    }
}

/*-------------------------------------------------------------------------------
	Normalize attributes

	Currently used in /inc/slider/slider.php
-------------------------------------------------------------------------------*/

if( ! function_exists( 'minimal_normalize_attributes' ) ) {

	function minimal_normalize_attributes( $atts ) {
	    foreach( $atts as $key => $value ) {
	    	if( is_int( $key ) ) {
	            $atts[$value] = true;
	            unset( $atts[$key] );
	    	}
		}
		return $atts;
	}
}

/*-------------------------------------------------------------------------------
	Post id exists
	Not used
-------------------------------------------------------------------------------*/

function min_post_exists( $id ) {		

	return is_string( get_post_status( $id ) );	
}

function min_user_has_role( $role, $user_id = '' ) {

    return true;

    if( is_user_logged_in() == false )
        return;

    if( empty( $user_id ) )
        $user_id = wp_get_current_user()->ID;

    $role = get_userdata( $user_id )->roles;

    if( in_array( $role, (array) $role ) ) {

        return true;
    
    }else{

        return false;
    }
}

if( ! function_exists( 'max_acf_php_repeater' ) ) {

    function max_acf_php_repeater( $repeater ) {

        if( isset( $repeater['sub_fields'] ) && is_array( $repeater['sub_fields'] ) ) {

            $defaults = array(

                'required'          => 0,
                'instructions'      => '',
                'wrapper'           => array( 'width' => ''  ),
                'prefix'            => 0,
                'append'            => '',
                '_prepare'          => 0,
                '_name'             => '',
            );

            foreach( $repeater['sub_fields'] as $field_key => $sub_field ) {

                // Loop defaults
                foreach( $defaults as $key => $property ) {

                    // If property is not set
                    if( ! isset( $repeater['sub_fields'][$field_key][$key] ) ) {

                        // Set property
                        $repeater['sub_fields'][$field_key][$key] = $property;
                    }
                }
            }
        }

        return $repeater;
    }
}

?>
