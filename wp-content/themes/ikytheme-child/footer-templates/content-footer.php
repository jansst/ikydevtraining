<?php
$footercolumn1 = get_field('footer_column_1', 'option');
$footercolumn2 = get_field('footer_column_2', 'option');
$footercolumn3 = get_field('footer_column_3', 'option');
?>


<footer id='main-footer'>
    <div class="row footerbar">
        <div class="full-row">
            <div class="blocks-container">
                <div class="footer-links-container">
                    <table class="footer-links">
                        <thead>
                        <tr>
                            <td><strong><a href="<?=$footercolumn1['column1']['url']?>"><?php echo $footercolumn1['column1']['title']; ?></a></strong></td>
                            <td><strong><a href="<?=$footercolumn2['column2']['url']?>"><?php echo $footercolumn2['column2']['title']; ?></a></strong></td>
                            <td><strong><a href="<?=$footercolumn3['column3']['url']?>"><?php echo $footercolumn3['column3']['title']; ?></a></strong></td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><a href="<?=$footercolumn1['row1']['url']?>"><?php echo $footercolumn1['row1']['title']; ?></a></td>
                            <td><a href="<?=$footercolumn2['row1']['url']?>"><?php echo $footercolumn2['row1']['title']; ?></a></td>
                            <td><a href="<?=$footercolumn3['row1']['url']?>"><?php echo $footercolumn3['row1']['title']; ?></a></td>
                        </tr>
                        <tr>
                            <td><a href="<?=$footercolumn1['row2']['url']?>"><?php echo $footercolumn1['row2']['title']; ?></a></td>
                            <td><a href="<?=$footercolumn2['row2']['url']?>"><?php echo $footercolumn2['row2']['title']; ?></a></td>
                            <td><a href="<?=$footercolumn3['row2']['url']?>"><?php echo $footercolumn3['row2']['title']; ?></a></td>
                        </tr>
                        <tr>
                            <td><a href="<?=$footercolumn1['row3']['url']?>"><?php echo $footercolumn1['row3']['title']; ?></a></td>
                            <td><a href="<?=$footercolumn2['row3']['url']?>"><?php echo $footercolumn2['row3']['title']; ?></a></td>
                            <td><a href="<?=$footercolumn3['row3']['url']?>"><?php echo $footercolumn3['row3']['title']; ?></a></td>
                        </tr>
                        <tr>
                            <td><a href="<?=$footercolumn1['row4']['url']?>"><?php echo $footercolumn1['row4']['title']; ?></a></td>
                            <td><a href="<?=$footercolumn2['row4']['url']?>"><?php echo $footercolumn2['row4']['title']; ?></a></td>
                            <td><a href="<?=$footercolumn3['row4']['url']?>"><?php echo $footercolumn3['row4']['title']; ?></a></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="footer-info-container">
                    <table class="footer-info">
                        <tr>
                            <td>
                                070 339 2425
                            </td>
                        </tr>
                        <tr>
                            <td>
                                info@ikycreative.nl
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Lange Kleiweg 52a,
                                2288 GK Rijswijk
                            </td>
                        </tr>
                    </table>
                    <img src="http://localhost:8080/wp-content/uploads/2019/03/iKY-logo-RGB@2x.png" />
                </div>
            </div>
        </div>
    </div>

    <div class="row credits">
        <div class="full-row">
            <div class="blocks-container">
                <span class="align-center">(C)Designed by He-sheng Z.</span>
            </div>
        </div>
    </div>
</footer>
