<?php
/**
 * @category 	Slider
 * @author 		Jasper
 * @since 		14-3-2019
 * @version 	1.0
 */

/**
 * Bruikbare functies (te overschrijven in functions.php)
 *
 * - minimal_slider_image_url(get_the_id())
 * - minimal_slider_image_id()
 * - the_content()
 * - the_title()
 * - the_author()
 * - the_ID()
 *
 * .slider-container:
 *		- .vertical-center
 *		- .horizontal-center
 * 
 */

?>

<?php if(minimal_slider_image_url(get_the_id() ) ) { 

$url = minimal_slider_image_url(get_the_id()); ?>

<li data-thumb="<?php echo $url; ?>" style='background-position: center;background-size: cover;background-repeat: no-repeat;background-image:url(<?php echo $url; ?>);'>


	<div class='slider-content-container vertical-center'>

		<div class='full-row'>

			<div class='slider-content'>


			</div>

		</div>

	</div>

</li>

<?php } ?>