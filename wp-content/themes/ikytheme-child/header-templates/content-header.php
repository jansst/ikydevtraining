<?php

// Retrieve settings from settings page
$sticky_menu = get_field('setting_sticky_header_name','option');
$main_menu = get_field('menu', 'option');


if($sticky_menu){

	$sticky_class = 'sticky-menu';
}else{

	$sticky_class = '';
}

?>

<div class="row header">
    <div class="full-row">
        <div class="blocks-container">
            <div class="site-logo-homepage">
                <img src="http://localhost:8080/wp-content/uploads/2019/03/iKY-logo-RGB.png"/>
            </div>
            <div class="mainmenu">
                <div class="menu">
                    <ul>
                        <li><a href="<?=$main_menu['link1']['url']?>"><?=$main_menu['link1']['title']?></a></li>
                        <li><a href="<?=$main_menu['link2']['url']?>"><?=$main_menu['link2']['title']?></a></li>
                        <li><a href="<?=$main_menu['link3']['url']?>"><?=$main_menu['link3']['title']?></a></li>
                        <li><a href="<?=$main_menu['link4']['url']?>"><?=$main_menu['link4']['title']?></a></li>
                        <li><a href="<?=$main_menu['zoekbalk']['url']?>"><?=$main_menu['zoekbalk']['title']?></a></li>
                        <li><a href="<?=$main_menu['telefoon']['url']?>"><?=$main_menu['telefoon']['title']?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
