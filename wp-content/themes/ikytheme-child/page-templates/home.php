<?php
    /* Template Name: Home */
$banner = get_field('banner', 'option');

get_header(); ?>

<div class="row banner">
    <div class="full-row">
        <div class="blocks-container">
            <div class="banner-container">
                <h1><?=$banner['banner_title'] ?></h1>
                <h3><?=$banner['banner_subtitle'] ?></h3>
                <p><?=$banner['banner_description'] ?></p>
                <a class="btn-begin btn-transparent" href="<?=$banner['get_started'] ?>">Get started -></a>
            </div>

        </div>
    </div>
</div>


<div class="row blocksection">
    <div class="full-row">
        <div class="blocks-container">


        </div>
    </div>
</div>

<div class="row description">
    <div class="full-row">
        <div class="blocks-container">


        </div>
    </div>
</div>

<div class="row slider">
    <div class="full-row">
        <div class="blocks-container">


        </div>
    </div>
</div>

<div class="row begin">
    <div class="full-row">
        <div class="blocks-container">


        </div>
    </div>
</div>

<?php get_footer(); ?>
