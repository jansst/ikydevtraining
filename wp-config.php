<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'acftraining' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'mysqlpass' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '#g#~se?R>:IPY=MT/U$m:EUW*rIDx76 ))hzurwoN-1gG{SF4X8IS:%%zGz]Xq7I' );
define( 'SECURE_AUTH_KEY',  't=a.Xt]g^aJY.,{}KpK}F(xE`bq<.~.]h_t11)DJgXUhzh5(]aZ~r_mUiJ 6]WQV' );
define( 'LOGGED_IN_KEY',    'Q9(@xe=?e}3i;|>9UOX>7UK#4?LUC5;#TKEXUf:A~Clh9 ;x={T`L.]Vouus%7by' );
define( 'NONCE_KEY',        '7l[bO,Yf1Wd[ET-[N&m|gGoz%ItL%JyEr+F0r/To7E`R%jdPTvsv>WXs%AfW$#%%' );
define( 'AUTH_SALT',        'BTC#s$ o?T%C `;ooR:~WyO(<OfJI+Khv<!^Q>g$HepUA/){`B(p,0twbJR[?.>)' );
define( 'SECURE_AUTH_SALT', 'Oqkx1p2`l{jjj~BAwc^]O 8F`df%1`e{,V4QQk!Ec<:i=:D<B~vG^@bR;(>X}ody' );
define( 'LOGGED_IN_SALT',   'rxyK7&@~&M&:b[qWO=*Bplu),H_`&i`bBY U](AxUDgQFM@7E!_$_%gc5Q 5t,zp' );
define( 'NONCE_SALT',       ']R3F{X-K?n,9>@+0if/}1{L7F6/{.aC;S$Ee6):-qHC_c?-k>Z+mmQA_u:SLHYif' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
